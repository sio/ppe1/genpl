/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#ifndef GENPL_SRC_PLAYLIST_H_
#define GENPL_SRC_PLAYLIST_H_

#include <chrono>
#include <string>
#include <list>

#include "Track.h"

namespace core {

/** \class Playlist
 */
class Playlist
{
 private:
  std::chrono::seconds duration;  ///< Playlist wanted duration in seconds (could be different from the one computed)
  std::string name;  ///< Playlist name. Also used to name the output filenames
  std::list<audio::Track> tracks;  ///< List of #audio::Track. Actually the core of the playlist

 public:
  /** \brief Default constructor
   *
   * Construct a blank playlist object.
   */
  explicit Playlist();

  /** \brief Detailed constructor
   *
   *
   * \param[in] _duration Duration you want the playlist to be long as
   *
   * \param[in] _name The name of your playlist (default to \c playlist)
   */
  explicit Playlist(std::chrono::seconds _duration,
                    std::string _name = "playlist");

  /** \brief Get the playlist duration
   *
   * \return Playlist duration in seconds
   */
  std::chrono::seconds getDuration();

  /** \brief Compute audio::Track list duration
   *
   * Compute the given track list duration by summing each element duration
   *
   * \note If empty list, no list or a \c nullptr is given, return always zero.
   *
   * \return Computed duration in seconds
   */
  static std::chrono::seconds computeDuration(const std::list<audio::Track> *_tracks);

  /** \brief Compute audio::Track list duration
   *
   * Compute the real duration (by summing each element duration) of
   * the object track list.
   *
   * \note If the list is empty, return always zero.
   *
   * \return Computed duration in seconds
   */
  std::chrono::seconds computeDuration();

  /** \brief Set the playlist duration
   *
   * Reset the playlist duration.
   *
   * \warning If the new duration is lower than the previous one, and
   * the #tracks list has a content greater than the new one, duration
   * is not updated!
   *
   * \param[in] _duration The new duration the playlist have to be long
   */
  void setDuration(std::chrono::seconds _duration);

  /** \brief Get playlist name
   *
   * \return Playlist name
   */
  std::string getName();

  /** \brief Set the playlist a new name
   *
   * \param[in] _name The new name
   */
  void setName(std::string _name);

  /** \brief Get a copy of the #tracks list
   *
   * \return The #tracks copy
   */
  std::list<audio::Track> getTracks();

  /** \brief Get the #tracks list reference
   *
   * \return A pointer to the #tracks list
   */
  std::list<audio::Track> *getTracksRef();

  /** \brief Set the new #tracks list
   *
   * Totally replace the existing #tracks list, even if existing with
   * content. No merging is done.
   *
   * \warning If the new list is greater than the defined duration, no
   * change is done.
   *
   * \warning If the new list is empty, no change is done. Use
   * resetTracks() instead.
   *
   * \param[in] _tracks The replacing audio::Track list
   */
  void setTracks(std::list<audio::Track> _tracks);

  /** \brief Reset the #tracks list to empty
   *
   * Clear the list and remove every element inside #tracks.
   *
   */
  void resetTracks();

  /** \brief Plus(+) operator
   *
   * Appends the given track to the end of the #tracks list
   *
   * \param[in] _track Track to be added at the end of the #tracks list
   *
   * \return Itself
   */
  Playlist operator+(audio::Track _track);

  /** \brief PlusEqual(+=) operator
   *
   * Appends the given track to the end of the #tracks list
   *
   * \param[in] _track Track to be added at the end of the #tracks list
   *
   * \return Itself
   */
  Playlist operator+=(audio::Track _track);

  /** \brief Plus(+) operator
   *
   * Appends the given track list elements to the end of the #tracks
   * list.
   *
   * \note No sorting is done after appending the given list to
   * #tracks
   *
   * \param[in] _tracks Track list to be added at the
   * end of the #tracks list
   *
   * \return Itself
   */
  Playlist operator+(std::list<audio::Track> _tracks);

  /** \brief PlusEqual(+=) operator
   *
   * Appends the given track list elements to the end of the #tracks
   * list.
   *
   * \note No sorting is done after appending the given list to
   * #tracks
   *
   * \param[in] _tracks Track list to be added at the end of the
   * #tracks list
   *
   * \return Itself
   */
  Playlist operator+=(std::list<audio::Track> _tracks);

  /** \brief Plus(+) operator
   *
   * Appends the given core::PLaylist track list elements to the end
   * of the #tracks list.
   *
   * \note No sorting is done after appending the given playlist to
   * #tracks
   *
   * \param[in] _playlist core::Playlist that track list is to be
   * added at the end of the #tracks list
   *
   * \return Itself
   */
  Playlist operator+(core::Playlist _playlist);

  /** \brief PlusEqual(+=) operator
   *
   * Appends the given core::PLaylist track list elements to the end
   * of the #tracks list.
   *
   * \note No sorting is done after appending the given playlist to
   * #tracks
   *
   * \param[in] _playlist core::Playlist that track list is to be
   * added at the end of the #tracks list
   *
   * \return Itself
   */
  Playlist operator+=(core::Playlist _playlist);

  /** \brief Write out the playlist as M3U format
   */
  void writeM3U();

  /** \brief Write out the playlist as XSPF format
   */
  void writeXSPF();

  /** \brief Print out a brief summary about the playlist
   */
  void summary();
};
}

#endif  // GENPL_SRC_PLAYLIST_H_
