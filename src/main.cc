/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

#ifdef HAVE_CONFIG_H
  #include "config.h"
  #define PROGNAME PACKAGE
#else
  #define PROGNAME "genpl"
#endif

static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PROGNAME));

#include "Analyzer.h"

int main(int argc, char **argv)
{
  cli::Analyzer *cli = cli::Analyzer::instance();
  cli->parse(argc, argv);
  cli->summary();
  cli->deleteInstance();
  return 0;
}
