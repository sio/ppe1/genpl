/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#ifndef GENPL_SRC_TRACK_H_
#define GENPL_SRC_TRACK_H_

#include <chrono>
#include <string>
#include <vector>
#include <list>
#include <cstdint>

#include <libpq-fe.h>

namespace audio {

/** \class Track
 *
 * \brief Track corresponding to the data model \c morceau
 *
 * \sa The data model in the PostgreSQL database
 * https://framagit.org/sio/ppe3/generateur-de-playlist/blob/master/RadioLibre.sql#L29
 */
class Track
{
 private:
  uint16_t id;  ///< Object identifier
  std::string title;  ///< Track title string
  std::chrono::seconds duration;  ///< Track duration in seconds
  std::string path;  ///< Track path string
  std::vector<std::string> artists;  ///< Collection of associated std::string artist
  std::vector<std::string> albums;  ///< Collection of associated std::string album

 public:
  /** \brief Default constructor
   *
   * With null #id, #title, #duration and #path
   */
  explicit Track();

  /** \brief Detailed constructor without relation
   *
   * Construct a #Track object based on the detailed parameterization,
   * but without any kind of relation with others audio:: classes
   *
   * \param[in] _id The #Track identification number
   *
   * \param[in] _title The #Track title string
   *
   * \param[in] _duration The #Track duration, in seconds
   *
   * \param[in] _path The #Track file path
   */
  explicit Track(uint16_t _id,
                 std::string _title,
                 std::chrono::seconds _duration,
                 std::string _path);

  /** \brief Detailed constructor
   *
   * Construct a #Track object based on the detailed parameterization
   *
   * \param[in] _id The #Track identification number
   *
   * \param[in] _title The #Track title string
   *
   * \param[in] _duration The #Track duration, in seconds
   *
   * \param[in] _path The #Track file path
   *
   * \param[in] _artist The #Track first artist collection element
   *
   * \param[in] _album The #Track first album collection element
   */
  explicit Track(uint16_t _id,
                 std::string _title,
                 std::chrono::seconds _duration,
                 std::string _path,
                 std::string _artist,
                 std::string _album);

  /** \brief Constructor from a PostgreSQL query result
   *
   * Construct a #Track object from the first query result row given
   * in parameter.
   *
   * \warning The pointer is not freed after construction. This is the
   * caller responsibility.
   * \code{.cpp}
   * PGconn * conn = ...;
   * PGresult * result = PQexec(conn, "SELECT id, nom, duree, chemin FROM morceau WHERE id=23;");
   * Track my_new_track(result);
   * PQclear(result);
   * \endcode
   *
   * \param[in] _track_result A pointer to the result of a PostgreSQL query
   */
  explicit Track(PGresult *_track_result);

  /** \brief Add two tracks
   *
   * The result is a std::list<audio::Track>
   *
   * \param[in] _plus_track The second audio::Track operand
   *
   * \return A std::list<audio::Track> containing only two elements,
   * this object first and the second operand after
   */
  std::list<Track> operator+(Track _plus_track);

  /** \brief Get the object's identifier
   *
   * \return A copy of the #id number
   */
  uint16_t getId();

  /** \brief Set the object's #id entification number
   *
   * \param[in] _id The number to use as identifier
   */
  void setId(uint16_t _id);

  /** \brief Get the object's #title
   *
   * \return A copy of the #title \c std::string
   */
  std::string getTitle();

  /** \brief Set the object's #title
   *
   * \param[in] _title The \c std::string to use as #title
   */
  void setTitle(std::string _title);

  /** \brief Get the object's duration
   *
   * \return A copy of the #duration value, in seconds
   */
  std::chrono::seconds getDuration();

  /** \brief Set the object's #duration value
   *
   * \param[in] _duration The value to use as duration, in seconds
   */
  void setDuration(std::chrono::seconds _duration);

  /** \brief Get the object's #path
   *
   * \return A copy of the #path \c std::string
   */
  std::string getPath();

  /** \brief Set the object's #path
   *
   * \param[in] _path The \c std::string to use as #path
   */
  void setPath(std::string _path);


  /** \brief Get the #artists container
   *
   * \return A copy of the #artists \c std::vector container
   */
  std::vector<std::string> getArtists();

  /** \brief Set the #artists container by replacing the existing one
   *
   * The \c std::vector container given as parameter \c _new_artists will
   * replace the #artists inplace.
   *
   * \param[in] _new_artists The replacing #artists container
   */
  void setArtists(std::vector<std::string> _new_artists);

  /** \brief Add a new artist to the #artists container
   *
   * \param[in] _artist The artist to be added to the #artists container as key
   */
  void addArtist(std::string _artist);


  /** \brief Get the #albums container
   *
   * \return A copy of the #albums \c std::vector container
   */
  std::vector<std::string> getAlbums();

  /** \brief Set the #albums container by replacing the existing one
   *
   * The \c std::vector container given as parameter \c _new_albums will
   * replace the #albums inplace.
   *
   * \param[in] _new_albums The replacing #albums container
   */
  void setAlbums(std::vector<std::string> _new_albums);

  /** \brief Add a new album to the #albums container
   *
   * \param[in] _album The album to be added to the #albums container as key
   */
  void addAlbum(std::string _album);
};
}

#endif  // GENPL_SRC_TRACK_H_
