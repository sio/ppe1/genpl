/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#ifndef GENPL_SRC_ANALYZER_H_
#define GENPL_SRC_ANALYZER_H_

#include <cstdint>
#include <string>
#include <chrono>
#include <map>

#include <log4cxx/logger.h>
#include <tclap/CmdLine.h>
#include <tclap/Constraint.h>

using namespace std::literals::chrono_literals;

namespace cli {

/** \class DurationConstraint
 *
 * This is class implement the TCLAP::Constraint interface
 */
class DurationConstraint : public TCLAP::Constraint<std::chrono::minutes>
{
 private:
  std::chrono::minutes min;  ///< Minimum value allowed for the duration, default to std::crono::minutes::zero()
  std::chrono::minutes max;  ///< Maximum value allowed for the duration, default to std::chrono::minutes::max()

 public:
  /** \brief Default constructor
   *
   * \param[in] _min Minimum value allowed for the duration, default
   * to std::crono::minutes::zero()
   *
   * \param[in] _max Maximum value allowed for the duration, default
   * to std::crono::minutes::max()
   */
  explicit DurationConstraint(std::chrono::minutes _min = std::chrono::minutes::zero(),
                              std::chrono::minutes _max = std::chrono::minutes::max());

  /** \brief Destructor
   */
  ~DurationConstraint();

  /** \brief Returns a description of the Constraint
   */
  std::string description() const;

  /** \brief Returns the short ID for the Constraint
   */
  std::string shortID() const;

  /** \brief Compute checking
   *
   * The method used to verify that the value parsed from the command
   * line meets the constraint.
   *
    \param[in] value The value that will be checked
   */
  bool check(const std::chrono::minutes &value) const;
};

/** \class Analyzer
 *
 * \brief Command Line Interface (CLI) analyzer
 *
 * This class is an helper to the CLI management. It contains options
 * and parameters waited on CLI, directly or through other classes.
 *
 * \note
 * Implements singleton design pattern
 */
class Analyzer
{
 private:
  static Analyzer *singleton;  ///< Pointer to the static singleton class member
  int argc;  ///< Keep track of the \c argc value passed when parsing
  char **argv;  ///< Keep track of the \c argv value passed when parsing
  TCLAP::CmdLine cmd;

  // Global options
  std::chrono::minutes duration;  ///< Requested playlist duration in minutes
  std::string name;  ///< Requested playlist name
  std::string loglevel;  ///< Runtime log level
  bool M3U_output;  ///< Requested playlist output to M3U format
  bool XSPF_output;  ///< Requested playlist output to XSPF format

  /** \brief Default constructor
   *
   * This constructor is not pubicly accessible. Use the instance()
   * member function instead to access the singleton
   */
  Analyzer();

  /** \brief Default destructor
   *
   * \note Please deleteInstance() before destruct
   */
  ~Analyzer();

 public:
  /** \brief Obtain singleton as pointer
   */
  static Analyzer *instance();

  /** \brief Remove singleton
   */
  static void deleteInstance();

  /** \brief Wrapper to TCLAP::CmdLine::setExceptionHandling()
   *
   * \param[in] _state Should CmdLine handle parsing exceptions internally?
   *
   * \note This is a convenience to help catching exceptions when
   * running tests.
   */
  void setExceptionHandling(const bool _state);

  /** \brief Analyze CLI and value accordingly
   */
  void parse(int _argc, char **_argv);

  /** \brief Print out summary
   */
  void summary() const;

  /** \brief Get the user defined playlist #duration
   *
   * \return The #duration in minutes as \c std::chrono::minutes
   */
  std::chrono::minutes getDuration();

  /** \brief Get the user defined playlist name
   *
   * \return The name
   */
  std::string getName();

  /** \brief Get the user defined log level
   *
   * \return The log level name string
   */
  std::string getLogLevel();

  /** \brief Get the user defined M3U export flag
   *
   * \return The M3U export flag
   */
  bool getM3U_output();

  /** \brief Get the user defined XSPF export flag
   *
   * \return The XSPF export flag
   */
  bool getXSPF_output();

  /** \brief Get the user defined \c argc counter
   *
   * \return The argument counter value
   */
  int getArgc();

  /** \brief Get the user defined \c argv array
   *
   * \return The \c argv array
   */
  char **getArgv();
};
}

#endif
