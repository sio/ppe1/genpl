/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#include "Playlist.h"

#include <stdexcept>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

#ifdef HAVE_CONFIG_H
  #include "config.h"
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PACKAGE));
#else
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("genpl"));
#endif

using namespace std::literals::chrono_literals;

core::Playlist::Playlist() :
  duration(0),
  name("noname")
{
  logger->debug("Construct empty Playlist");
}

core::Playlist::Playlist(std::chrono::seconds _duration,
                         std::string _name) :
  duration(_duration),
  name(_name)
{
  logger->debug("Construct Playlist with name=" + name + " and duration=" + std::to_string(duration.count()));
}

std::chrono::seconds core::Playlist::getDuration()
{
  return duration;
}

std::chrono::seconds core::Playlist::computeDuration(const std::list<audio::Track> *_tracks)
{
  std::chrono::seconds tracks_duration_sum = 0s;

  if(_tracks != nullptr)
  {
    for(audio::Track track : *_tracks)
    {
      tracks_duration_sum += track.getDuration();
    }
  }

  return tracks_duration_sum;
}

std::chrono::seconds core::Playlist::computeDuration()
{
  std::chrono::seconds tracks_duration_sum = 0s;

  for(audio::Track track : tracks)
  {
    tracks_duration_sum += track.getDuration();
  }

  return tracks_duration_sum;
}

void core::Playlist::setDuration(std::chrono::seconds _duration)
{
  duration = _duration;
}

std::string core::Playlist::getName()
{
  return name;
}

void core::Playlist::setName(std::string _name)
{
  name = _name;
}

std::list<audio::Track> core::Playlist::getTracks()
{
  return tracks;
}

std::list<audio::Track> *core::Playlist::getTracksRef()
{
  return &tracks;
}

void core::Playlist::setTracks(std::list<audio::Track> _tracks)
{
  tracks = _tracks;
}

void core::Playlist::resetTracks()
{
  tracks.clear();
}

core::Playlist core::Playlist::operator+(audio::Track _track)
{
  *this += _track;
  return *this;
}

core::Playlist core::Playlist::operator+=(audio::Track _track)
{
  if(computeDuration() + _track.getDuration() < duration)
  {
    tracks.push_back(_track);
    logger->debug("Add a " + std::to_string(_track.getDuration().count()) + "s track to playlist '" + name + "'");
  }
  else
  {
    throw std::out_of_range("Overload track sum requested.");
  }

  return *this;
}

core::Playlist core::Playlist::operator+(std::list<audio::Track> _tracks)
{
  *this += _tracks;
  return *this;
}

core::Playlist core::Playlist::operator+=(std::list<audio::Track> _tracks)
{
  if(! _tracks.empty())
  {
    if(computeDuration() + computeDuration(&_tracks) < duration)
    {
      tracks.splice(tracks.end(), _tracks);
    }
    else
    {
      throw std::out_of_range("Overload track list sum requested.");
    }
  }
  else
  {
    logger->debug("Track list to add is empty");
  }

  return *this;
}

core::Playlist core::Playlist::operator+(core::Playlist _playlist)
{
  *this += _playlist;
  return *this;
}

core::Playlist core::Playlist::operator+=(core::Playlist _playlist)
{
  *this += _playlist.getTracks();
  return *this;
}

void core::Playlist::writeM3U()
{
  logger->debug("Write Playlist to M3U");
}

void core::Playlist::writeXSPF()
{
  logger->debug("Write Playlist to XSPF");
}

void core::Playlist::summary()
{
  std::ostringstream output;
  output << std::endl
         << "[Playlist summary (all times in seconds)]" << std::endl
         << "  Name: " << name << std::endl
         << "  Requested duration: " << duration.count() << std::endl
         << "  Computed track list duration: " << computeDuration().count() << std::endl
         << "  Remaining time in playlist: " << (duration - computeDuration()).count() << std::endl
         << "  Number of tracks: " << tracks.size() << std::endl;
  logger->info(output.str());
}
