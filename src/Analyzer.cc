/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#include "Analyzer.h"

#include <ostream>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

#ifdef HAVE_CONFIG_H
  #include "config.h"
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PACKAGE));
#else
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("genpl"));
#endif

cli::Analyzer *cli::Analyzer::singleton = nullptr;

namespace TCLAP {
template<>
struct ArgTraits<std::chrono::minutes>
{
  typedef ValueLike ValueCategory;
};

template <>
void ExtractValue<std::chrono::minutes>(std::chrono::minutes &out,
                                        const std::string &in,
                                        ValueLike)
{
  out = std::chrono::minutes(std::stoi(in));
};
}

cli::DurationConstraint::DurationConstraint(std::chrono::minutes _min,
    std::chrono::minutes _max) :
  min(_min),
  max(_max)
{}

cli::DurationConstraint::~DurationConstraint()
{}

std::string cli::DurationConstraint::description() const
{
  std::ostringstream output;
  output << "duration value must be in [" << min.count() << ".." << max.count() << "]";
  return output.str();
}

std::string cli::DurationConstraint::shortID() const
{
  std::ostringstream output;
  output << "INTEGER [" << min.count() << ".." << max.count() << "]";
  return output.str();
}

bool cli::DurationConstraint::check(const std::chrono::minutes &value) const
{
  return (value >= min) and (value <= max);
}

cli::Analyzer *cli::Analyzer::instance()
{
  if(!singleton)
  {
    singleton = new Analyzer();
  }

  return singleton;
}

void cli::Analyzer::deleteInstance()
{
  delete singleton;
}

void cli::Analyzer::setExceptionHandling(const bool _state)
{
  cmd.setExceptionHandling(_state);
}

cli::Analyzer::Analyzer() :
  argc(0),
  argv(nullptr),
  cmd("Automated playlist generator, based on criteria and with M3U and XSPF output format",
      ' ',
#ifdef PACKAGE_VERSION
      PACKAGE_VERSION),
#else
      "local-version"),
#endif
  duration(0),
  name(),
  loglevel(log4cxx::Level::getError()->toString()),
  M3U_output(true),
  XSPF_output(false)
{
  if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
  {
    log4cxx::BasicConfigurator::configure();
  }

  cmd.setExceptionHandling(true);
}

cli::Analyzer::~Analyzer()
{
  singleton = nullptr;
}

void cli::Analyzer::parse(int _argc, char **_argv)
{
  argc = _argc;
  argv = _argv;
  // Global options
  cli::DurationConstraint duration_Constraint(1min, 24h);
  TCLAP::UnlabeledValueArg<std::chrono::minutes> duration_UnlblArg("duration",
      "Playlist duration in minutes",
      true, 0min, &duration_Constraint, cmd);
  TCLAP::ValueArg<std::string> name_Arg("", "name",
                                        "Playlist name",
                                        false, std::string("playlist"), "TEXT", cmd);
  std::vector<std::string> loglevels;
  loglevels.push_back(log4cxx::Level::getDebug()->toString());
  loglevels.push_back(log4cxx::Level::getInfo()->toString());
  loglevels.push_back(log4cxx::Level::getError()->toString());
  loglevels.push_back(log4cxx::Level::getFatal()->toString());
  TCLAP::ValuesConstraint<std::string> allowed_loglevels(loglevels);
  TCLAP::ValueArg<std::string> loglevel_Arg(
    "", "loglevel",
    "Loglevel, default to " + log4cxx::Level::getDebug()->toString(),
    false, log4cxx::Level::getDebug()->toString(), &allowed_loglevels, cmd);
  TCLAP::SwitchArg M3U_output_SwArg(
    "", "M3U",
    "Output to M3U (default, even if not specified)",
    cmd, true);
  TCLAP::SwitchArg XSPF_output_SwArg(
    "", "XSPF",
    "Output to XSPF",
    cmd, false);
  // Parse the args.
  cmd.parse(argc, argv);
  // Get the value parsed by each arg.
  duration = duration_UnlblArg.getValue();
  name = name_Arg.getValue();
  loglevel = loglevel_Arg.getValue();
  logger->setLevel(log4cxx::Level::toLevel(loglevel));
  M3U_output = M3U_output_SwArg.getValue();
  XSPF_output = XSPF_output_SwArg.getValue();
}

void cli::Analyzer::summary() const
{
  std::ostringstream output;
  output << std::endl << std::boolalpha
         << "[Arguments and flags summary]" << std::endl
         << "  Duration: " << duration.count() << std::endl
         << "  Name: " << name << std::endl
         << "  Loglevel: " << loglevel << std::endl
         << "  M3U output: " << M3U_output << std::endl
         << "  XSPF output: " << XSPF_output << std::endl;
  logger->info(output.str());
}

std::chrono::minutes cli::Analyzer::getDuration()
{
  return duration;
}

std::string cli::Analyzer::getName()
{
  return name;
}

std::string cli::Analyzer::getLogLevel()
{
  return loglevel;
}

bool cli::Analyzer::getM3U_output()
{
  return M3U_output;
}

bool cli::Analyzer::getXSPF_output()
{
  return XSPF_output;
}

int cli::Analyzer::getArgc()
{
  return argc;
}

char **cli::Analyzer::getArgv()
{
  return argv;
}
