/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#include "Track.h"

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

#ifdef HAVE_CONFIG_H
  #include "config.h"
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PACKAGE));
#else
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("genpl"));
#endif


audio::Track::Track() :
  id(0),
  title(),
  duration(0),
  path()
{
  logger->debug("Construct empty Track");
}

audio::Track::Track(uint16_t _id,
                    std::string _title,
                    std::chrono::seconds _duration,
                    std::string _path) :
  id(_id),
  title(_title),
  duration(_duration),
  path(_path)
{
  logger->debug("Construct Track with id=" + std::to_string(id) + ", title='" + title + "', duration=" + std::to_string(duration.count()) + ", path='" + path + "'");
}

audio::Track::Track(uint16_t _id,
                    std::string _title,
                    std::chrono::seconds _duration,
                    std::string _path,
                    std::string _artist,
                    std::string _album) :
  id(_id),
  title(_title),
  duration(duration),
  path(_path)
{
  artists.push_back(_artist);
  albums.push_back(_album);
  logger->debug("Construct Track with id=" + std::to_string(id) + ", title='" + title + "', duration=" + std::to_string(duration.count()) + ", path='" + path + "', artist='" + _artist + "' and album='" + _album + "'");
}

audio::Track::Track(PGresult *_track_result) :
  id(0),
  title(),
  duration(0),
  path()
{
  logger->debug("Construct Playlist with PGresult*");
}

std::list<audio::Track> audio::Track::operator+(Track _plus_track)
{
  std::list<audio::Track> _list;
  _list.push_back(*this);
  _list.push_back(_plus_track);
  return _list;
}

uint16_t audio::Track::getId()
{
  return id;
}

void audio::Track::setId(uint16_t _id)
{
  id = _id;
}

std::string audio::Track::getTitle()
{
  return title;
}

void audio::Track::setTitle(std::string _title)
{
  title = _title;
}

std::chrono::seconds audio::Track::getDuration()
{
  return duration;
}

void audio::Track::setDuration(std::chrono::seconds _duration)
{
  duration =  _duration;
}

std::string audio::Track::getPath()
{
  return path;
}

void audio::Track::setPath(std::string _path)
{
  path = _path;
}

std::vector<std::string> audio::Track::getArtists()
{
  return artists;
}

void audio::Track::setArtists(std::vector<std::string> _new_artists)
{
  artists = _new_artists;
}

void audio::Track::addArtist(std::string _artist)
{
  artists.push_back(_artist);
}

std::vector<std::string> audio::Track::getAlbums()
{
  return albums;
}

void audio::Track::setAlbums(std::vector<std::string> _new_albums)
{
  albums = _new_albums;
}

void audio::Track::addAlbum(std::string _album)
{
  albums.push_back(_album);
}
