/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "Analyzer.h"

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#ifdef HAVE_CONFIG_H
  #include "config.h"
  #define PROGNAME PACKAGE"-unittest-Analyzer"
#else
  #define PROGNAME "genpl-unittest-Analyzer"
#endif

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PROGNAME));

using namespace std::literals::chrono_literals;

class test_Analyzer : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_Analyzer);
  CPPUNIT_TEST(testInstanceSingleton);
  CPPUNIT_TEST(testParser_null_duration);
  CPPUNIT_TEST(testParser_negative_integer_duration);
  CPPUNIT_TEST(testParser_negative_float_duration);
  CPPUNIT_TEST(testParser_positive_float_duration);
  CPPUNIT_TEST(testParser_positive_integer_duration);
  CPPUNIT_TEST(testParser_outofrange_duration);
  CPPUNIT_TEST_SUITE_END();

 private:
  cli::Analyzer *cli;

 public:
  void setUp()
  {
    cli = cli::Analyzer::instance();
    cli->setExceptionHandling(false);

    if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
    {
      log4cxx::BasicConfigurator::configure();
    }

    logger->setLevel(log4cxx::Level::getDebug());
  };

  void tearDown()
  {
    cli->deleteInstance();
  };

  void testInstanceSingleton()
  {
    cli::Analyzer *cli_second = cli::Analyzer::instance();
    CPPUNIT_ASSERT_EQUAL(cli, cli_second);
  };

  void testParser_null_duration()
  {
    char *argv[] = {PROGNAME,
                    "0"
                   };
    CPPUNIT_ASSERT_THROW(cli->parse(sizeof(argv) / sizeof(char *), argv),
                         TCLAP::CmdLineParseException);
  };

  void testParser_negative_integer_duration()
  {
    char *argv[] = {PROGNAME,
                    "-10"
                   };
    CPPUNIT_ASSERT_THROW(cli->parse(sizeof(argv) / sizeof(char *), argv),
                         TCLAP::CmdLineParseException);
  };

  void testParser_negative_float_duration()
  {
    char *argv[] = {PROGNAME,
                    "-10.4"
                   };
    CPPUNIT_ASSERT_THROW(cli->parse(sizeof(argv) / sizeof(char *), argv),
                         TCLAP::CmdLineParseException);
  };

  void testParser_positive_float_duration()
  {
    char *argv[] = {PROGNAME,
                    "6.66"
                   };
    cli->parse(sizeof(argv) / sizeof(char *), argv);
    CPPUNIT_ASSERT(cli->getDuration() == 6min);
  };

  void testParser_positive_integer_duration()
  {
    char *argv[] = {PROGNAME,
                    "10"
                   };
    cli->parse(sizeof(argv) / sizeof(char *), argv);
    CPPUNIT_ASSERT(cli->getDuration() == 10min);
  };

  void testParser_outofrange_duration()
  {
    char *argv[] = {PROGNAME,
                    "1441"
                   };
    CPPUNIT_ASSERT_THROW(cli->parse(sizeof(argv) / sizeof(char *), argv),
                         TCLAP::CmdLineParseException);
  };
};

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(test_Analyzer);


int main()
{
  // Get the top level suite from the registry
  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
  CppUnit::Test *suite = registry.makeTest();
  // Adds the test to the list of test to run
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(suite);
  // Change the default outputter to a compiler error format outputter
  runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
                      std::cerr));
  // Run the tests.
  bool wasSucessful = runner.run();
  // Return error code 1 if the one of test failed.
  return wasSucessful ? 0 : 1;
}
