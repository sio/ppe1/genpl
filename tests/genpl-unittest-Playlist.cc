/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "Playlist.h"
#include "Track.h"

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#ifdef HAVE_CONFIG_H
  #include "config.h"
  #define PROGNAME PACKAGE"-unittest-Playlist"
#else
  #define PROGNAME "genpl-unittest-Playlist"
#endif

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PROGNAME));

using namespace std::literals::chrono_literals;

class test_Playlist : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_Playlist);
  CPPUNIT_TEST(testPlaylist_create);
  CPPUNIT_TEST(testPlaylist_operators);
  CPPUNIT_TEST(testPlaylist_computed_duration);
  CPPUNIT_TEST(testPlaylist_outofrange_addition);
  CPPUNIT_TEST_SUITE_END();

 public:
  void setUp()
  {
    if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
    {
      log4cxx::BasicConfigurator::configure();
    }

    logger->setLevel(log4cxx::Level::getDebug());
  };

  void tearDown() {};

  void testPlaylist_create()
  {
    std::chrono::seconds duration(123min);
    std::string name("create");
    core::Playlist playlist(duration, name);
    CPPUNIT_ASSERT_EQUAL(playlist.getName(), name);
    CPPUNIT_ASSERT_EQUAL(playlist.getDuration().count(), duration.count());
  };

  void testPlaylist_operators()
  {
    std::chrono::seconds duration(123min);
    std::string name("operators");
    core::Playlist playlist(duration, name);
    audio::Track track(0, "Track title", 120s, "/track/file/path");
    audio::Track track_last(666, "eltit kcarT", 210s, "/track/file/PATH");
    track.addAlbum("Album title");
    track_last.addAlbum("Devil album");
    playlist = playlist + track;
    CPPUNIT_ASSERT_EQUAL(playlist.getTracks().front().getId(), track.getId());
    playlist += track + track_last;
    CPPUNIT_ASSERT_EQUAL(playlist.getTracks().back().getId(), track_last.getId());
    playlist += track;
    CPPUNIT_ASSERT_EQUAL(playlist.getTracks().back().getId(), track.getId());
    size_t playlist_size(playlist.getTracks().size());
    playlist += playlist.getTracks();
    CPPUNIT_ASSERT_EQUAL(playlist.getTracks().size(), playlist_size * 2);
    playlist_size = playlist.getTracks().size();
    playlist += std::list<audio::Track>();
    CPPUNIT_ASSERT_EQUAL(playlist.getTracks().size(), playlist_size);
  };

  void testPlaylist_computed_duration()
  {
    uint16_t id(1);
    std::string title("Super titre");
    std::chrono::seconds track_duration(3min + 28s);
    std::string path("/track/file/path");
    audio::Track track(id, title, track_duration, path);
    track.addAlbum("Album title");
    std::chrono::seconds duration(123min);
    std::string name("computed_duration");
    core::Playlist playlist(duration, name);
    size_t nb_tracks(10);

    for(size_t i = 0; i < nb_tracks; ++i)
    {
      playlist += track;
    }

    CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(playlist.computeDuration().count()), track_duration.count() * nb_tracks);
  };

  void testPlaylist_outofrange_addition()
  {
    uint16_t id(1);
    std::string title("Super titre");
    std::chrono::seconds track_duration(6min + 54s);
    std::string path("/track/file/path");
    audio::Track track(id, title, track_duration, path);
    track.addAlbum("Album title");
    std::chrono::seconds duration(1h + 11min);
    std::string name("outofrange");
    core::Playlist playlist(duration, name);

    for(size_t i = 0; i < duration.count() / track_duration.count(); ++i)
    {
      playlist += track;
    }

    CPPUNIT_ASSERT_THROW(playlist += track,
                         std::out_of_range);
  };
};

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(test_Playlist);

int main()
{
  // Get the top level suite from the registry
  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
  CppUnit::Test *suite = registry.makeTest();
  // Adds the test to the list of test to run
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(suite);
  // Change the default outputter to a compiler error format outputter
  runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
                      std::cerr));
  // Run the tests.
  bool wasSucessful = runner.run();
  // Return error code 1 if the one of test failed.
  return wasSucessful ? 0 : 1;
}
