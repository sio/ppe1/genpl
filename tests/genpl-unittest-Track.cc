/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
*/

#include "Track.h"

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#ifdef HAVE_CONFIG_H
  #include "config.h"
  #define PROGNAME PACKAGE"-unittest-Track"
#else
  #define PROGNAME "genpl-unittest-Track"
#endif

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PROGNAME));

using namespace std::literals::chrono_literals;

class test_Track : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_Track);
  CPPUNIT_TEST(testTrack_create);
  CPPUNIT_TEST(testTrack_create_empty);
  CPPUNIT_TEST(testTrack_add);
  //  CPPUNIT_TEST(testTrack_);
  CPPUNIT_TEST_SUITE_END();

 public:
  void setUp()
  {
    if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
    {
      log4cxx::BasicConfigurator::configure();
    }

    logger->setLevel(log4cxx::Level::getDebug());
  };

  void tearDown() {};

  void testTrack_create()
  {
    uint16_t id(1);
    std::string title("Super titre");
    std::chrono::seconds duration(3min + 28s);
    std::string path("/track/file/path");
    std::string artist("Super artiste");
    std::string album("Super album");
    audio::Track track(id, title, duration, path);
    size_t nb_artists(3);
    size_t nb_albums(12);

    for(auto i = 0; i < nb_artists; ++i)
    {
      track.addArtist(artist);
    }

    for(auto i = 0; i < nb_albums; ++i)
    {
      track.addAlbum(album);
    }

    CPPUNIT_ASSERT_EQUAL(track.getId(), id);
    CPPUNIT_ASSERT_EQUAL(track.getTitle(), title);
    CPPUNIT_ASSERT_EQUAL(track.getDuration().count(), duration.count());
    CPPUNIT_ASSERT_EQUAL(track.getPath(), path);
    CPPUNIT_ASSERT_EQUAL(track.getArtists().size(), nb_artists);
    CPPUNIT_ASSERT_EQUAL(track.getAlbums().size(), nb_albums);
    CPPUNIT_ASSERT_EQUAL(track.getArtists()[0], artist);
    CPPUNIT_ASSERT_EQUAL(track.getAlbums()[0], album);
  };

  void testTrack_create_empty()
  {
    audio::Track track;
    CPPUNIT_ASSERT(track.getTitle().empty());
  };

  void testTrack_add()
  {};
};

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(test_Track);

int main()
{
  // Get the top level suite from the registry
  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
  CppUnit::Test *suite = registry.makeTest();
  // Adds the test to the list of test to run
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(suite);
  // Change the default outputter to a compiler error format outputter
  runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
                      std::cerr));
  // Run the tests.
  bool wasSucessful = runner.run();
  // Return error code 1 if the one of test failed.
  return wasSucessful ? 0 : 1;
}
